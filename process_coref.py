def find_hero_sentences(file_name, hero_names):
    fin = open("./coreference/coref_" + file_name + ".txt", 'r')

    line = fin.readline().rstrip().split()
    num_sentences = set()
    process = False
    while line != []:
        line = fin.readline().rstrip().split()
        for name in hero_names:
            if name in line:
                process = True

        while line != [] and not (len(line) == 5 and line[1:] == ["Sentence", "Head","Text","Context"]):    # program separator
            if process:
               num_sentences.add(int(line[0]))

            line = fin.readline().rstrip().split()
        process = False

    fin.close()
    return num_sentences

def get_sentences_by_num(file_name, num_sentences):
    fin = open("sentences_from_chapters/sentences_" + file_name + ".txt", 'r')

    data = ['']
    line = fin.readline().rstrip()
    while line != '':
        line = fin.readline().rstrip()
        arr = line.split()
        add = ''
        while line != '' and not (len(arr) == 4 and arr[0] == "Sentence" and arr[1][0] == '#'):
            add += line + ' '
            line = fin.readline().rstrip()
            arr = line.split()
        data += [add]

    fin.close()
    result_sent = []
    numbers = sorted(num_sentences)

    for elem in numbers:
        result_sent.append(data[elem])

    return result_sent[:]


if __name__ == "__main__":
    #file_name = "__"
    #hero = "Frodo"
    print("Input:\nfile_name\nchapters num\nhero names (the first - the most common)\n")

    file_name = input()
    ch_num = int(input())

    hero_names = input().split()
    hero = hero_names[0]

    fout = open("./hero_" + hero + "/" + hero + file_name + "_sentences.txt", 'w')

    for i in range(ch_num):
        f_name = file_name + str(i + 1)
        nums = sorted(find_hero_sentences(f_name, hero_names))
        print(i + 1, nums)
        print(' '.join(get_sentences_by_num(f_name, nums)), file= fout)

