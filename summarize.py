from gensim.summarization import summarize

print("Input hero name [the most common]")
hero = input()
print("Summarizing starts\n")

file_name1 = "./hero_" + hero + "/" + hero + "_LOTR1_chapter_sentences.txt"
fin1 = open(file_name1, 'r')
file_name2 = "./hero_" + hero + "/" + hero + "_LOTR1_book2_chapter_sentences.txt"
fin2 = open(file_name2, 'r')

file_name3 = "./hero_" + hero + "/" + hero + "_LOTR2_chapter_sentences.txt"
fin3 = open(file_name3, 'r')
file_name4 = "./hero_" + hero + "/" + hero + "_LOTR2_book4_chapter_sentences.txt"
fin4 = open(file_name4, 'r')

file_name5 = "./hero_" + hero + "/" + hero + "_LOTR3_chapter_sentences.txt"
fin5 = open(file_name5, 'r')
file_name6 = "./hero_" + hero + "/" + hero + "_LOTR3_book6_chapter_sentences.txt"
fin6 = open(file_name6, 'r')


text = fin1.read() + '\n' + fin2.read() + fin3.read() + '\n' + fin4.read() + fin5.read() + '\n' + fin6.read()
print(summarize(text, ratio=0.05))
#print(summarize(text, word_count=1000))