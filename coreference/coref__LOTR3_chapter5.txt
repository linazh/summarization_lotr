1. Sentence	Head	Text	Context
1	1 (gov)	Chapter 5 THE RIDE OF THE ROHIRRIM It was dark and Merry could see nothing as he lay on the ground	
3	4	it	
2. Sentence	Head	Text	Context
1	4 (gov)	THE RIDE OF THE ROHIRRIM	
1	8	ROHIRRIM It	
3. Sentence	Head	Text	Context
1	7 (gov)	THE ROHIRRIM	
1	38	him	
2	1	He	
2	3	his	
3	2	he	
6	19	the Rohirrim	
4. Sentence	Head	Text	Context
292	3 (gov)	Merry	
1	12	Merry	
1	17	he	
15	7	Merry	
18	1	Merry	
27	1	Merry	
27	9	he	
28	5	his	
30	1	Merry	
30	3	he	
30	7	a tall Rider like Eomer	
30	21	his	
31	1	He	
32	2	he	
32	9	he	
41	2	Merry	
57	1	Merry	
57	26	him	
59	1	He	
60	1	He	
61	2	he	
65	1	Merry	
65	4	he	
65	7	him	
65	13	he	
67	6	Merry	
68	1	His	
68	13	he	
147	1	Merry	
149	7	him	
150	2	he	
150	15	his	
165	20	Merry	
230	9	Merry	
230	29	he	
232	1	Merry	
262	1	Merry	
262	16	he	
262	20	his	
263	1	He	
264	4	he	
266	9	Merry	
273	1	Merry	
275	1	He	
275	15	he	
286	1	Merry himself	
286	16	him	
287	1	His	
293	4	his	
5. Sentence	Head	Text	Context
262	21 (gov)	his sword	
262	23	its	
6. Sentence	Head	Text	Context
1	21 (gov)	the ground	
63	12	the ground	
7. Sentence	Head	Text	Context
1	30 (gov)	the night	
26	12	the night	
140	3	night	
141	1	It	
141	7	it	
207	32	night	
224	3	night	
8. Sentence	Head	Text	Context
32	17 (gov)	the trees	
1	40	trees	
60	20	the trees	
92	12	trees , men in the dark	
92	12	trees	
157	31	trees	
165	10	the trees	
9. Sentence	Head	Text	Context
2	4 (gov)	his head	
161	16	his head	
215	13	his head	
10. Sentence	Head	Text	Context
264	2 (gov)	this	
265	1	It	
11. Sentence	Head	Text	Context
3	8 (gov)	a sound like faint drums in the wooded hills and mountain-steps	
298	3	that sound	
12. Sentence	Head	Text	Context
199	9 (gov)	the faint drums	
3	11	faint drums in the wooded hills and mountain-steps	
13. Sentence	Head	Text	Context
4	2 (gov)	The throb	
5	1	He	
6	1	He	
6	8	he	
6	13	him	
7	1	He	
9	3	he	
10	1	He	
10	19	his	
11	1	He	
11	6	he	
11	15	he	
11	23	his	
12	1	He	
14. Sentence	Head	Text	Context
4	15 (gov)	some other point	
5	8	it	
15. Sentence	Head	Text	Context
6	5 (gov)	them	
7	13	their	
16. Sentence	Head	Text	Context
267	1 (gov)	Wild	
267	15	it	
17. Sentence	Head	Text	Context
268	2 (gov)	The orcs busy about the walls	
268	13	they	
18. Sentence	Head	Text	Context
7	5 (gov)	the horses	
283	2	The horses	
19. Sentence	Head	Text	Context
7	8 (gov)	the dark	
33	9	the dark	
92	17	the dark	
20. Sentence	Head	Text	Context
269	6 (gov)	the north-gate in the Rammas the king halted	
270	7	him	
270	10	him	
21. Sentence	Head	Text	Context
7	14 (gov)	their shiftings	
7	16	their	
22. Sentence	Head	Text	Context
8	35 (gov)	East Anorien	
157	22	Anorien	
23. Sentence	Head	Text	Context
8	2 (gov)	The host	
19	9	the host	
201	16	the host who have ridden down to Mundburg in days of peace	
305	8	the host	
24. Sentence	Head	Text	Context
46	5 (gov)	Druadan Forest	
8	26	the Druadan Forest	
46	7	it	
25. Sentence	Head	Text	Context
112	1 (gov)	Road	
8	32	the great road	
23	8	the road	
24	28	the road	
99	14	the road	
113	6	it	
129	16	the road	
158	25	the road	
161	9	the road	
203	6	the road	
203	7	it	
204	15	the road	
225	6	the road	
26. Sentence	Head	Text	Context
10	14 (gov)	the ever-deepening gloom	
179	4	this gloom	
27. Sentence	Head	Text	Context
10	20 (gov)	his heart	
200	4	no heart in all the	
287	2	His heart	
28. Sentence	Head	Text	Context
11	24 (gov)	his lord	
39	3	my lord	
54	9	my lord	
56	1	He	
207	58	our lord	
29. Sentence	Head	Text	Context
12	9 (gov)	the old King	
12	12	he	
30. Sentence	Head	Text	Context
14	8 (gov)	Dernhelm	
15	1	He	
15	4	his	
15	14	he	
16	1	He	
16	9	Dernhelm	
17	1	Dernhelm	
17	4	no comfort	
17	6	he	
230	12	Dernhelm	
230	15	his	
262	5	Dernhelm	
271	1	Dernhelm	
31. Sentence	Head	Text	Context
275	10 (gov)	the dark plain	
305	32	the plain	
32. Sentence	Head	Text	Context
14	10 (gov)	Elfhelm	
38	13	Elfhelm	
44	6	Elfhelm	
209	3	Elfhelm	
210	15	he	
255	1	Elfhelm	
271	9	Elfhelm	
33. Sentence	Head	Text	Context
14	13 (gov)	the Marshal who commanded the eored in which they were riding	
35	8	Elfhelm the Marshal	
34. Sentence	Head	Text	Context
15	5 (gov)	his men	
24	22	men	
157	30	men	
157	34	their	
223	17	men	
276	31	men	
278	8	his men	
35. Sentence	Head	Text	Context
277	9 (gov)	the Black Captain	
277	25	him	
277	28	his	
36. Sentence	Head	Text	Context
278	2 (gov)	After a	
279	2	they	
37. Sentence	Head	Text	Context
20	8 (gov)	ride from the out-walls of Minas Tirith that encircled the townlands	
302	4	ride now	
306	4	ride now	
38. Sentence	Head	Text	Context
20	11 (gov)	the out-walls of Minas Tirith that encircled the townlands	
265	11	the out-walls	
266	1	They	
266	4	them ; too soon for Merry	
39. Sentence	Head	Text	Context
20	14 (gov)	Minas Tirith that encircled the townlands	
150	11	Minas Tirith	
284	16	Minas Tirith	
40. Sentence	Head	Text	Context
20	18 (gov)	the townlands	
203	28	the townlands	
41. Sentence	Head	Text	Context
99	3 (gov)	our scouts	
21	1	Scouts	
99	6	they	
100	5	them	
161	4	scouts	
210	2	The scouts	
42. Sentence	Head	Text	Context
23	1 (gov)	Others hastening back	
23	14	them	
43. Sentence	Head	Text	Context
24	11 (gov)	three	
24	34	three	
44. Sentence	Head	Text	Context
24	16 (gov)	Amon Din	
113	20	Din	
156	22	Amon Din	
156	38	Din	
170	7	Din	
170	15	he	
170	17	his	
45. Sentence	Head	Text	Context
24	2 (gov)	A host of the enemy	
24	9	it	
46. Sentence	Head	Text	Context
24	5 (gov)	the enemy	
40	3	the enemy	
44	9	the enemy	
129	10	the enemy	
217	9	the enemy	
258	4	the enemy	
47. Sentence	Head	Text	Context
308	6 (gov)	Snowmane	
284	6	Snowmane	
48. Sentence	Head	Text	Context
48	25 (gov)	the orcs	
25	1	Orcs	
182	17	his orcs	
49. Sentence	Head	Text	Context
44	17 (gov)	the hills	
25	6	the hills and woods along the roadside	
136	32	the hills	
152	27	the hills	
291	19	the hills	
50. Sentence	Head	Text	Context
51	12 (gov)	the king	
26	2	The king and Eomer	
61	18	the king	
116	4	the king	
154	16	the king	
160	2	The king	
167	6	the king	
168	7	he	
191	12	the king	
208	4	the king	
229	2	The king	
229	12	his	
229	15	him	
234	5	the king	
271	6	the king	
278	5	the king	
278	7	his	
284	3	the king	
285	1	He	
286	2	himself	
298	9	the king	
299	4	he	
299	11	his	
299	13	he	
308	3	the king	
309	2	him	
309	3	his	
309	19	he	
310	2	him	
310	7	his	
310	11	he	
51. Sentence	Head	Text	Context
29	2 (gov)	Poor Pippin	
27	12	Pippin	
150	5	Pippin	
52. Sentence	Head	Text	Context
63	5 (gov)	Eomer	
30	9	Eomer	
76	11	Eomer	
88	2	Eomer	
89	14	his	
90	12	he	
101	9	Eomer	
116	1	Eomer	
130	2	Eomer	
133	12	he	
133	14	his	
135	14	Eomer	
145	13	Eomer	
161	1	Eomer	
161	15	his	
162	7	he	
178	2	Eomer	
180	9	his	
182	16	his	
207	18	Eomer , I counsel that we rest now , and set out hence by night , and so time our	
207	18	Eomer	
207	21	I counsel that we rest now , and set out hence by night , and so time our	
212	2	Eomer	
222	14	Eomer	
253	1	Eomer , my son !	
253	1	Eomer	
253	4	my son	
311	1	Eomer	
311	9	his	
311	13	his	
312	2	he	
53. Sentence	Head	Text	Context
42	4 (gov)	those their drums	
31	8	the drums that were beating again	
54. Sentence	Head	Text	Context
32	4 (gov)	voices speaking low	
232	3	voices	
55. Sentence	Head	Text	Context
33	1 (gov)	Men nearby	
34	9	him , cursing the tree-roots	
34	9	him	
35	1	He	
36	9	he	
56. Sentence	Head	Text	Context
297	4 (gov)	a searing second	
297	5	it	
297	15	its	
57. Sentence	Head	Text	Context
35	4 (gov)	the voice of Elfhelm the Marshal	
68	2	His voice	
68	31	it	
89	15	his voice	
58. Sentence	Head	Text	Context
36	5 (gov)	a tree-root , Sir	
36	1	I	
36	5	a tree-root	
37	11	me	
39	2	my	
43	1	I	
43	5	I	
53	3	I	
54	3	I	
54	6	myself with my lord	
54	8	my	
91	1	I	
92	1	I	
131	13	I	
164	8	me	
182	21	my	
185	3	I	
207	20	I	
217	2	I	
241	1	I	
241	14	my	
241	19	me	
253	3	my	
264	18	my	
59. Sentence	Head	Text	Context
39	7 (gov)	we	
39	10	ourselves	
60. Sentence	Head	Text	Context
301	11 (gov)	a sword-day	
304	3	he	
304	10	his	
304	14	he	
61. Sentence	Head	Text	Context
303	1 (gov)	Ride to Gondor !	
304	2	that	
307	1	Ride to Gondor !	
62. Sentence	Head	Text	Context
42	3 (gov)	their	
43	8	them	
43	20	them	
45	14	they	
46	1	They	
47	6	they	
48	1	They	
48	14	they	
48	27	they	
49	6	they	
49	13	they	
49	23	they	
50	2	they	
50	5	their	
63. Sentence	Head	Text	Context
304	18 (gov)	such a blast	
304	20	it	
304	22	it	
64. Sentence	Head	Text	Context
47	19 (gov)	the beasts	
106	20	beasts	
124	23	beasts	
65. Sentence	Head	Text	Context
48	7 (gov)	Gondor	
49	18	it	
146	10	Gondor	
182	7	Gondor	
182	10	it	
205	7	Gondor	
214	9	Gondor	
276	13	Gondor	
303	3	Gondor	
307	3	Gondor	
66. Sentence	Head	Text	Context
48	10 (gov)	the Mark	
122	24	the Mark	
67. Sentence	Head	Text	Context
48	19 (gov)	the darkness	
124	7	the Darkness	
127	3	it	
129	4	it	
230	20	the darkness	
260	7	no darkness	
297	27	the darkness	
68. Sentence	Head	Text	Context
49	2 (gov)	us	
49	10	us	
69. Sentence	Head	Text	Context
310	5 (gov)	the knights of his house	
310	15	them	
70. Sentence	Head	Text	Context
50	8 (gov)	Theoden	
63	3	Theoden	
63	32	his	
63	37	his	
64	1	He	
64	17	his	
98	8	Theoden	
117	3	Theoden	
118	7	he	
128	2	Theoden	
139	2	Theoden	
185	13	Theoden	
220	2	Theoden	
247	2	Theoden	
248	1	He	
248	7	his	
248	14	he	
248	33	him : Now	
254	8	Theoden	
279	8	Theoden	
280	3	he	
291	2	Theoden	
291	7	his	
299	40	Theoden	
311	33	Theoden	
71. Sentence	Head	Text	Context
312	1 (gov)	Fey he	
312	9	his	
313	1	His	
314	21	his	
72. Sentence	Head	Text	Context
312	7 (gov)	the battle-fury of his fathers	
312	16	his	
312	20	he	
73. Sentence	Head	Text	Context
312	10 (gov)	his fathers	
315	28	them	
315	31	they	
315	44	them	
316	13	they	
316	16	they	
316	26	them	
316	32	their	
74. Sentence	Head	Text	Context
112	9 (gov)	Wild Men	
57	9	wild men	
74	2	Wild Men	
75	2	Wild Men	
90	2	Wild Men	
106	18	Wild Men	
108	1	They	
109	2	Wild Men	
109	4	they	
110	1	They	
111	1	They	
114	2	Wild Men	
115	17	Wild Men	
124	12	Wild Men	
132	2	Wild Men	
152	3	The Wild Men	
153	9	they	
163	2	Wild Men	
164	1	They	
168	2	Wild Men	
189	6	Wild Men , answered Ghan	
189	6	Wild Men	
191	18	them	
200	15	the Wild Men	
200	23	they	
75. Sentence	Head	Text	Context
313	3 (gov)	His golden shield	
314	1	it	
76. Sentence	Head	Text	Context
60	10 (gov)	pursuit of the last lantern	
60	16	it	
77. Sentence	Head	Text	Context
153	2 (gov)	The light	
62	19	light	
78. Sentence	Head	Text	Context
63	3 (gov)	Theoden and Eomer	
63	9	them on the ground	
79. Sentence	Head	Text	Context
66	3 (gov)	one of those old images brought to life	
66	1	Here	
134	6	here	
171	6	here	
80. Sentence	Head	Text	Context
66	28 (gov)	the forgotten craftsmen long ago	
226	6	the	
228	1	They	
81. Sentence	Head	Text	Context
67	14 (gov)	the Wild Man	
78	7	the Wild Man	
104	4	the Wild Man	
105	5	he	
106	1	He	
117	8	the Wild Man	
123	19	the Wild Man	
134	2	Wild Man	
239	6	the Wild Man	
82. Sentence	Head	Text	Context
67	23 (gov)	some question	
67	25	it	
83. Sentence	Head	Text	Context
69	1 (gov)	No	
69	7	he	
84. Sentence	Head	Text	Context
69	3 (gov)	father of Horse-men	
126	6	father of Horse-men	
85. Sentence	Head	Text	Context
73	1 (gov)	We	
73	4	we	
76	2	our	
77	8	us	
79	1	We	
80	1	We	
86. Sentence	Head	Text	Context
75	6 (gov)	Stone-houses ; before Tall Men come up out of Water	
96	8	Stone-houses	
87. Sentence	Head	Text	Context
79	5 (gov)	hills	
108	3	hills	
156	33	hills that from Nardol to Din ran east	
88. Sentence	Head	Text	Context
82	6 (gov)	now inside too	
248	35	Now	
89. Sentence	Head	Text	Context
85	7 (gov)	far-away	
85	9	he	
90. Sentence	Head	Text	Context
196	6 (gov)	his eyes	
89	9	flat face and dark eyes	
91. Sentence	Head	Text	Context
91	6 (gov)	Ghan-buri-Ghan	
103	2	Ghan-buri-Ghan	
122	6	Ghan-buri-Ghan	
125	1	Ghan-buri-Ghan	
126	1	He	
126	4	himself	
126	12	he	
126	20	him	
193	1	Ghan-buri-Ghan	
193	9	his	
194	2	he	
195	3	he	
196	5	his	
198	1	he	
92. Sentence	Head	Text	Context
92	14 (gov)	men in the dark	
94	1	They	
93. Sentence	Head	Text	Context
93	8 (gov)	ten	
274	5	ten	
274	18	it	
94. Sentence	Head	Text	Context
99	2 (gov)	our	
100	1	We	
101	3	we	
95. Sentence	Head	Text	Context
201	22 (gov)	Mundburg	
102	1	Mundburg	
96. Sentence	Head	Text	Context
107	2 (gov)	Many paths	
155	27	paths	
97. Sentence	Head	Text	Context
110	6 (gov)	Rimmon	
113	16	Rimmon	
98. Sentence	Head	Text	Context
314	12 (gov)	the grass	
113	10	grass and tree	
99. Sentence	Head	Text	Context
115	26 (gov)	the wild woods	
124	16	the woods	
185	10	the woods	
100. Sentence	Head	Text	Context
116	1 (gov)	Eomer and the king	
116	8	their	
101. Sentence	Head	Text	Context
118	1 (gov)	We	
119	3	we	
120	7	we	
122	9	we	
102. Sentence	Head	Text	Context
119	6 (gov)	a host of foes behind , what matter	
136	10	our host	
103. Sentence	Head	Text	Context
119	8 (gov)	foes behind	
236	16	foes	
104. Sentence	Head	Text	Context
120	3 (gov)	the Stone-city	
121	2	it	
121	9	itself	
105. Sentence	Head	Text	Context
123	2 (gov)	Dead men	
123	5	friends to living men	
123	8	living men	
123	12	them	
175	4	their friends	
106. Sentence	Head	Text	Context
124	3 (gov)	you	
124	21	them like beasts	
107. Sentence	Head	Text	Context
131	1 (gov)	We	
131	10	us	
108. Sentence	Head	Text	Context
141	14 (gov)	Ghan	
132	9	Ghan	
154	12	old Ghan	
161	13	old Ghan	
165	18	old Ghan	
166	4	Ghan	
167	2	Ghan	
176	4	Ghan	
189	9	Ghan	
109. Sentence	Head	Text	Context
155	42 (gov)	the hidden Stonewain Valley	
133	9	Stonewain Valley	
159	20	the Stonewain Valley	
110. Sentence	Head	Text	Context
133	6 (gov)	four horses in Stonewain Valley yonder	
155	23	their horses	
111. Sentence	Head	Text	Context
133	15 (gov)	his hand	
215	4	his hand	
112. Sentence	Head	Text	Context
135	2 (gov)	we	
135	17	we	
136	5	us	
136	9	our	
136	28	we	
113. Sentence	Head	Text	Context
135	11 (gov)	the leaders	
156	9	the leaders	
114. Sentence	Head	Text	Context
142	2 (gov)	Sun	
301	19	the sun	
314	8	the Sun	
314	8	Sun	
115. Sentence	Head	Text	Context
142	4 (gov)	we	
145	2	we	
146	3	we	
116. Sentence	Head	Text	Context
142	6 (gov)	her	
142	10	she	
143	2	she	
117. Sentence	Head	Text	Context
143	5 (gov)	East-mountains	
144	1	It	
144	4	the opening of day in the sky-fields	
118. Sentence	Head	Text	Context
151	5 (gov)	that day	
144	6	day in the sky-fields	
228	13	the day	
119. Sentence	Head	Text	Context
148	5 (gov)	the last stage before the battle	
148	1	This	
149	1	It	
149	14	it	
214	1	This , lord : they were errand-riders of Gondor ; Hirgon was one maybe .	
120. Sentence	Head	Text	Context
150	5 (gov)	Pippin and the flames	
151	13	they of the enemy waiting to waylay them	
151	20	them	
121. Sentence	Head	Text	Context
157	20 (gov)	the City	
153	15	the beleaguered city	
218	6	the City	
236	2	The City	
281	2	The City	
281	5	now nearer	
296	21	the City	
316	43	the City	
122. Sentence	Head	Text	Context
153	19 (gov)	the Riders	
155	17	the Riders	
155	22	their	
155	34	their	
158	7	the Riders	
158	8	their	
158	14	they	
158	22	them	
158	48	themselves	
123. Sentence	Head	Text	Context
155	2 (gov)	The start	
155	11	it	
156	1	It	
124. Sentence	Head	Text	Context
288	1 (gov)	Time	
155	14	time for the Riders	
207	36	so time	
223	5	this time	
125. Sentence	Head	Text	Context
156	14 (gov)	wide grey thickets	
158	3	the thickets	
126. Sentence	Head	Text	Context
156	28 (gov)	a great gap in the line of hills that from Nardol to Din ran east	
157	3	the gap	
157	40	it	
127. Sentence	Head	Text	Context
157	35 (gov)	their way	
157	37	it	
204	5	that way	
128. Sentence	Head	Text	Context
158	65 (gov)	Mindolluin	
226	9	Mindolluin	
129. Sentence	Head	Text	Context
158	12 (gov)	cover before they went into open battle ; for beyond them lay the road and the plains of Anduin , while east and southwards the slopes were bare and rocky , as the writhen hills gathered themselves together and climbed up	
158	54	bastion upon bastion	
130. Sentence	Head	Text	Context
159	3 (gov)	The leading company	
229	10	the leading company	
255	5	your company	
261	3	The leading company	
261	13	it	
131. Sentence	Head	Text	Context
159	9 (gov)	those	
159	21	they	
132. Sentence	Head	Text	Context
160	5 (gov)	the captains	
165	2	The captains	
166	1	They	
208	9	the captains	
133. Sentence	Head	Text	Context
165	6 (gov)	then out of the trees	
165	24	them	
134. Sentence	Head	Text	Context
168	5 (gov)	many things	
191	4	these things	
135. Sentence	Head	Text	Context
169	1 (gov)	First	
248	30	first	
254	4	first	
270	2	first	
311	21	first	
136. Sentence	Head	Text	Context
171	9 (gov)	Stone-folk s	
173	9	them	
174	1	They	
174	9	them	
175	1	They	
175	3	their	
137. Sentence	Head	Text	Context
171	11 (gov)	new walls	
173	1	Walls	
268	6	the walls	
138. Sentence	Head	Text	Context
176	3 (gov)	that old	
176	12	it	
139. Sentence	Head	Text	Context
277	22 (gov)	as yet no tidings	
177	2	Good tidings	
186	7	tidings	
140. Sentence	Head	Text	Context
180	1 (gov)	Our	
180	7	us	
181	10	us	
183	9	us	
184	2	we	
184	8	we	
141. Sentence	Head	Text	Context
180	5 (gov)	devices oft	
181	4	itself	
142. Sentence	Head	Text	Context
181	3 (gov)	The accursed darkness itself	
181	8	a cloak to us	
143. Sentence	Head	Text	Context
183	2 (gov)	The out-wall	
217	13	the out-wall	
238	14	the out-wall	
144. Sentence	Head	Text	Context
189	3 (gov)	No other words	
239	8	words	
145. Sentence	Head	Text	Context
191	5 (gov)	we	
191	15	we	
192	3	we	
146. Sentence	Head	Text	Context
192	7 (gov)	tomorrow	
207	46	tomorrow	
207	51	it	
147. Sentence	Head	Text	Context
193	7 (gov)	the earth	
296	18	the earth	
148. Sentence	Head	Text	Context
195	15 (gov)	a strange air	
241	22	the air	
282	8	the air	
149. Sentence	Head	Text	Context
196	2 (gov)	A light	
198	6	that	
198	12	it	
150. Sentence	Head	Text	Context
198	33 (gov)	Rohan	
225	10	Rohan	
276	6	Rohan	
305	22	Rohan	
316	7	Rohan	
151. Sentence	Head	Text	Context
198	10 (gov)	a twinkling as it seemed	
198	15	he	
198	17	his	
152. Sentence	Head	Text	Context
201	1 (gov)	We	
203	2	we	
203	17	us	
203	22	we	
206	1	We	
207	3	we	
207	14	our	
207	23	we	
207	37	our	
207	40	we	
207	57	our	
153. Sentence	Head	Text	Context
201	8 (gov)	Elfhelm ; for there are riders in the host who have ridden down to Mundburg in days of peace	
202	1	I for one .	
154. Sentence	Head	Text	Context
201	24 (gov)	days of peace	
241	8	days of peace	
155. Sentence	Head	Text	Context
203	25 (gov)	the wall of the townlands	
233	12	the wall	
245	12	the wall	
255	13	the wall	
272	15	the wall	
156. Sentence	Head	Text	Context
205	3 (gov)	that stretch	
206	4	it	
157. Sentence	Head	Text	Context
205	5 (gov)	the errand-riders of Gondor	
205	11	their	
214	5	they	
214	7	errand-riders of Gondor	
216	12	they	
216	17	they	
217	6	they	
217	20	they	
217	31	they	
217	41	their	
218	1	They	
221	8	our riding	
221	14	our coming	
158. Sentence	Head	Text	Context
205	13 (gov)	their greatest speed	
311	14	his speed	
159. Sentence	Head	Text	Context
207	61 (gov)	the signal	
279	11	no signal	
160. Sentence	Head	Text	Context
210	19 (gov)	two	
210	23	two	
210	27	two	
213	3	it	
217	26	two	
161. Sentence	Head	Text	Context
214	11 (gov)	Hirgon	
215	3	his	
215	12	his	
162. Sentence	Head	Text	Context
214	1 (gov)	This	
216	2	this also	
216	5	it	
217	4	it	
217	17	it	
217	23	that	
163. Sentence	Head	Text	Context
222	7 (gov)	yet late	
290	2	Too late	
292	5	it	
164. Sentence	Head	Text	Context
223	2 (gov)	mayhap in this time	
224	1	It	
165. Sentence	Head	Text	Context
225	8 (gov)	the host of Rohan	
276	4	the host of Rohan	
166. Sentence	Head	Text	Context
227	22 (gov)	the great mountain	
227	26	it	
167. Sentence	Head	Text	Context
230	28 (gov)	last	
280	2	last	
292	7	last	
168. Sentence	Head	Text	Context
233	1 (gov)	Out-riders	
234	1	They	
169. Sentence	Head	Text	Context
236	12 (gov)	the field	
259	18	the field	
170. Sentence	Head	Text	Context
238	10 (gov)	few left upon the out-wall	
238	17	they	
171. Sentence	Head	Text	Context
246	6 (gov)	Widfara	
241	12	Widfara	
261	21	Widfara	
172. Sentence	Head	Text	Context
242	3 (gov)	the wind	
309	8	the wind , white horse	
309	8	the wind	
173. Sentence	Head	Text	Context
243	4 (gov)	a breath	
243	15	it	
174. Sentence	Head	Text	Context
243	8 (gov)	the South	
295	8	the South	
175. Sentence	Head	Text	Context
243	13 (gov)	a sea-tang in it	
243	19	it	
176. Sentence	Head	Text	Context
244	2 (gov)	The morning	
275	21	morning	
315	5	morning	
177. Sentence	Head	Text	Context
244	6 (gov)	new things	
259	14	things	
178. Sentence	Head	Text	Context
248	5 (gov)	the men of his household who were near , and he spoke now in a clear voice so that many also of the riders of the first eored heard him : Now is the hour come	
248	46	sons of Eorl	
179. Sentence	Head	Text	Context
248	31 (gov)	the first eored	
254	5	the first eored	
254	11	it	
270	3	The first eored	
180. Sentence	Head	Text	Context
248	38 (gov)	the hour come	
305	25	that hour	
181. Sentence	Head	Text	Context
249	1 (gov)	Foes and fire	
251	8	them	
182. Sentence	Head	Text	Context
250	4 (gov)	you	
251	2	ye	
183. Sentence	Head	Text	Context
250	9 (gov)	an alien field	
250	12	the glory	
184. Sentence	Head	Text	Context
309	4 (gov)	his banner	
254	18	banner in the centre	
309	21	it	
185. Sentence	Head	Text	Context
255	8 (gov)	the right when we pass the wall	
271	16	the right	
186. Sentence	Head	Text	Context
256	2 (gov)	Grimbold	
256	5	his	
272	1	Grimbold	
187. Sentence	Head	Text	Context
257	4 (gov)	the other companies	
257	13	they	
188. Sentence	Head	Text	Context
259	3 (gov)	we	
259	9	we	
			
