1. Sentence	Head	Text	Context
265	6 (gov)	one	
265	11	that	
2. Sentence	Head	Text	Context
1	4 (gov)	A CONSPIRACY	
241	41	our conspiracy	
274	5	our conspiracy	
3. Sentence	Head	Text	Context
1	7 (gov)	we	
1	12	ourselves	
2	17	we	
4. Sentence	Head	Text	Context
63	2 (gov)	Merry	
1	15	Merry	
3	1	They	
4	9	them	
7	2	them	
7	19	them	
9	1	Merry	
10	1	Merry	
11	8	them	
38	1	Merry	
40	11	they	
41	3	they	
44	2	Merry	
52	16	Merry	
65	1	They	
65	9	their	
66	7	they	
66	14	their	
67	2	This they	
68	3	they	
93	1	Merry	
93	3	them	
103	2	Merry	
108	1	Merry	
108	18	themselves	
124	1	Merry	
129	2	Merry	
147	8	Merry	
160	12	Merry , if I had not seen that black shape on the landing-stage and heard the queer sound in Maggot s voice	
160	12	Merry	
167	9	Merry	
176	12	Merry	
177	1	Merry	
183	9	Merry	
207	2	Merry	
225	1	Merry and I	
234	7	Merry firmly	
239	7	Merry	
264	10	Merry	
273	8	Merry	
280	2	Merry	
294	11	Merry	
320	1	Merry	
320	9	they	
344	9	Merry	
350	20	Merry	
369	5	Merry	
378	2	Merry	
402	12	Merry	
5. Sentence	Head	Text	Context
2	3 (gov)	something funny about all this	
2	13	it	
6. Sentence	Head	Text	Context
2	9 (gov)	I	
53	2	I	
55	1	I	
56	2	s	
58	9	I	
62	2	me	
82	5	I	
91	1	I	
91	5	I	
102	2	me	
120	28	my	
127	10	I	
134	1	I	
138	3	mine	
140	3	me	
141	8	I	
148	3	me	
149	1	I	
149	16	me	
150	1	I	
150	11	I	
150	28	me	
150	30	that	
151	11	that	
156	7	I	
160	1	I	
160	15	I	
169	1	I	
171	1	I	
172	29	me	
173	1	I	
173	21	I	
180	1	I	
181	1	I	
182	2	I	
183	1	I	
183	3	I	
183	18	myself	
186	4	my	
198	9	I	
198	18	I	
198	24	that	
203	1	I	
203	3	I	
204	1	I	
205	6	my	
209	18	I	
212	1	I	
213	7	I	
214	1	I	
214	13	I	
218	2	I	
220	17	me	
221	10	me	
221	15	me	
225	3	I	
227	1	My	
229	2	I	
230	1	I	
233	1	I	
240	1	My	
241	1	I	
241	29	I	
241	34	my	
242	1	I	
242	13	I	
242	17	I	
243	7	I	
243	12	I	
247	10	I	
247	20	I	
252	1	I	
252	6	I	
252	13	myself	
252	21	I	
253	1	I	
255	1	I	
256	2	that	
256	3	I	
256	5	my	
257	4	I	
257	7	I	
258	12	I	
258	16	my	
259	1	I	
259	12	you Frodo , that has ever seen the old fellow s secret book	
264	5	I	
265	2	I	
267	1	I	
268	1	I	
273	4	I	
273	11	I	
273	13	my	
273	16	myself	
276	13	I	
282	7	I	
283	4	I	
289	2	I	
289	7	you , Mr. Frodo	
292	7	I	
305	1	I	
306	1	I	
306	8	I	
307	1	I	
311	11	I	
312	1	I	
313	9	I	
314	4	I	
314	13	I	
315	1	I	
317	1	That	
335	1	I	
336	1	I	
336	8	I	
336	30	I	
337	4	me	
338	2	I	
339	1	I	
345	1	I	
353	13	I	
358	1	I	
358	5	my	
359	1	I	
360	2	I	
360	17	that	
361	2	I	
361	7	my	
370	7	I	
382	1	I	
385	1	I	
385	12	I	
385	25	my	
385	31	I	
386	3	I	
386	27	I	
395	1	I	
395	17	I	
396	12	I	
396	27	me	
397	1	I	
399	4	I	
401	9	me	
404	1	I	
7. Sentence	Head	Text	Context
268	5 (gov)	another look	
269	4	it , Frodo	
269	4	it	
8. Sentence	Head	Text	Context
4	3 (gov)	hundred	
4	7	it	
18	38	hundred	
9. Sentence	Head	Text	Context
4	12 (gov)	the river-bank	
351	28	the river-bank	
10. Sentence	Head	Text	Context
4	20 (gov)	a broad wooden landing-stage	
160	24	the landing-stage	
11. Sentence	Head	Text	Context
5	4 (gov)	A large flat ferry-boat	
5	8	it	
32	2	The ferry-boat	
12. Sentence	Head	Text	Context
6	14 (gov)	two	
340	5	two	
363	15	two	
13. Sentence	Head	Text	Context
6	6 (gov)	the water	
7	17	the water before them	
32	7	the water	
14. Sentence	Head	Text	Context
6	12 (gov)	the light of two lamps	
74	12	no light	
15. Sentence	Head	Text	Context
6	15 (gov)	two lamps	
13	1	Lamps	
16. Sentence	Head	Text	Context
274	2 (gov)	we	
274	4	our	
274	9	we	
274	19	we	
276	9	our	
17. Sentence	Head	Text	Context
7	4 (gov)	the mists in the flat fields	
35	21	the mists	
18. Sentence	Head	Text	Context
276	11 (gov)	our chief investigator	
276	16	him	
277	3	he	
19. Sentence	Head	Text	Context
7	36 (gov)	the bank	
12	6	the bank	
48	11	the bank	
20. Sentence	Head	Text	Context
9	4 (gov)	the pony	
38	13	the pony	
21. Sentence	Head	Text	Context
9	11 (gov)	the ferry	
37	7	the ferry	
22. Sentence	Head	Text	Context
9	15 (gov)	the others	
175	2	The others	
175	14	themselves	
309	7	the others	
23. Sentence	Head	Text	Context
12	4 (gov)	the other side	
12	12	it a	
14	11	it	
26	13	that side	
24. Sentence	Head	Text	Context
281	1 (gov)	Here	
282	2	he	
282	13	he	
283	8	he	
283	12	himself	
25. Sentence	Head	Text	Context
65	4 (gov)	Buck Hill	
14	6	the Buck Hill	
26. Sentence	Head	Text	Context
14	22 (gov)	many round windows	
73	36	round windows	
74	17	the windows	
27. Sentence	Head	Text	Context
15	7 (gov)	Brandy Hall	
15	11	the ancient home of the Brandybucks	
17	8	Brandy Hall , changed his name to Brandybuck , and settled down to become master of what was virtually a small independent country	
17	8	Brandy Hall	
18	17	Brandy Hall	
21	15	Brandy Hall	
22	20	the Hall	
353	30	the Hall	
28. Sentence	Head	Text	Context
15	14 (gov)	the Brandybucks	
19	2	The Brandybucks	
19	4	their	
72	11	the Brandybucks	
74	2	they	
77	1	They	
77	7	themselves	
78	1	They	
78	16	them	
379	2	The Brandybucks	
379	10	them	
29. Sentence	Head	Text	Context
16	4 (gov)	Gorhendad Oldbuck	
16	9	Oldbuck	
17	1	He	
17	11	his	
18	1	His	
30. Sentence	Head	Text	Context
16	23 (gov)	Shire	
187	6	Shire	
205	4	Shire	
259	9	Shire	
387	16	Shire	
387	24	it	
31. Sentence	Head	Text	Context
16	10 (gov)	the Oldbuck family	
18	2	His family	
18	9	his	
388	2	His family	
388	16	he	
389	1	His	
390	1	He	
390	14	him	
32. Sentence	Head	Text	Context
16	18 (gov)	the Marish	
22	5	the Marish	
143	17	the Marish	
33. Sentence	Head	Text	Context
16	23 (gov)	the Shire	
20	26	the Shire	
31	18	the Shire	
38	34	the Shire	
43	4	the Shire	
187	6	the Shire , of course	
187	6	the Shire	
259	9	the Shire	
387	16	the Shire	
34. Sentence	Head	Text	Context
289	15 (gov)	no wrong to you , Mr. Frodo , nor to Mr. Gandalf	
290	1	He	
290	16	he	
35. Sentence	Head	Text	Context
16	28 (gov)	the river	
16	28	the river , which was the original boundary of the land eastwards	
20	14	the river	
28	18	the river	
34	15	the river	
51	5	the river	
36. Sentence	Head	Text	Context
17	14 (gov)	Brandybuck	
22	27	Brandybuck	
37. Sentence	Head	Text	Context
290	4 (gov)	some sense	
290	6	mind you	
292	2	it	
294	1	It	
358	6	my mind	
38. Sentence	Head	Text	Context
17	29 (gov)	a small independent country	
67	17	the country	
70	15	the country	
39. Sentence	Head	Text	Context
18	28 (gov)	three	
105	6	three	
318	1	Three	
40. Sentence	Head	Text	Context
18	10 (gov)	his days	
20	1	That	
20	4	the origin of Buckland , a thickly inhabited strip between the river and the Old Forest , a sort of colony from the Shire	
21	1	Its	
41. Sentence	Head	Text	Context
18	20 (gov)	the whole of the low hill	
399	3	the whole	
42. Sentence	Head	Text	Context
295	4 (gov)	us	
296	5	us	
297	6	us	
300	1	We	
301	1	We	
302	1	We	
302	6	we	
43. Sentence	Head	Text	Context
295	16 (gov)	the bitter end	
296	16	it	
44. Sentence	Head	Text	Context
20	6 (gov)	Buckland	
20	11	a thickly inhabited strip between the river and the Old Forest , a sort of colony from the Shire	
65	23	Buckland	
354	6	Buckland	
361	10	Buckland	
362	25	Buckland	
45. Sentence	Head	Text	Context
20	18 (gov)	Old Forest	
28	30	the Forest	
30	2	The Forest	
373	11	Old Forest	
397	7	Old Forest	
400	9	the Forest	
412	16	the forest	
46. Sentence	Head	Text	Context
297	19 (gov)	a word	
299	4	it	
47. Sentence	Head	Text	Context
20	18 (gov)	the Old Forest	
169	12	the Old Forest	
365	10	the Old Forest	
373	11	the Old Forest	
385	8	the Old Forest	
385	19	it	
48. Sentence	Head	Text	Context
21	5 (gov)	Bucklebury , clustering in the banks and slopes behind Brandy Hall	
21	3	Its chief village	
21	5	Bucklebury	
21	7	clustering in the banks and slopes behind Brandy Hall	
65	17	Bucklebury	
341	8	Bucklebury	
343	1	It	
49. Sentence	Head	Text	Context
22	10 (gov)	the Bucklanders	
23	12	the Bucklanders	
24	9	they	
25	6	they	
25	15	them	
26	1	Their	
26	14	they	
31	2	The Bucklanders	
31	4	their	
50. Sentence	Head	Text	Context
22	17 (gov)	the Master of the Hall -LRB- as the head of the Brandybuck family was called -RRB-	
353	27	the Master of the Hall	
51. Sentence	Head	Text	Context
22	24 (gov)	the head of the Brandybuck family	
36	4	his head	
241	35	my head	
52. Sentence	Head	Text	Context
303	17 (gov)	the Elves advice	
304	16	it	
305	5	it	
306	5	it	
53. Sentence	Head	Text	Context
304	1 (gov)	Gildor	
312	4	Gildor	
337	2	Gildor	
339	6	Gildor	
339	10	he	
54. Sentence	Head	Text	Context
23	2 (gov)	most of the folk of the old Shire	
23	19	it	
55. Sentence	Head	Text	Context
24	20 (gov)	Four	
37	2	four	
38	4	it	
56. Sentence	Head	Text	Context
24	17 (gov)	the other hobbits of the Four Farthings	
37	3	The four hobbits	
142	1	Hobbits	
57. Sentence	Head	Text	Context
309	1 (gov)	he	
311	1	he	
311	8	his	
58. Sentence	Head	Text	Context
29	11 (gov)	a complete protection	
26	2	Their land	
27	1	It	
27	17	it	
28	1	It	
29	6	it	
59. Sentence	Head	Text	Context
351	23 (gov)	the Hedge	
26	18	a hedge : the High Hay	
30	7	the hedge	
252	25	the hedge	
60. Sentence	Head	Text	Context
388	23 (gov)	the Brandywine Bridge	
28	8	Brandywine Bridge	
52	9	Brandywine Bridge	
65	29	the Bridge	
351	35	the Bridge	
362	7	the Bridge	
61. Sentence	Head	Text	Context
53	10 (gov)	the Brandywine	
28	33	the Brandywine	
54	8	it	
62. Sentence	Head	Text	Context
318	5 (gov)	Captain Frodo and company	
319	1	they	
319	5	they	
63. Sentence	Head	Text	Context
320	6 (gov)	a song	
321	1	It	
64. Sentence	Head	Text	Context
34	1 (gov)	Sam	
34	5	the only member of the party who had not been over the river before	
35	1	He	
35	14	his	
36	1	He	
36	3	his	
158	1	Sam	
226	1	Sam	
226	20	he	
226	25	his	
279	4	Sam	
280	5	Sam	
284	1	Sam	
287	2	Sam	
293	1	Sam	
293	4	him	
303	8	Sam	
305	12	Sam , who was now grinning	
305	12	Sam	
65. Sentence	Head	Text	Context
247	8 (gov)	the Party	
34	8	the party who had not been over the river before	
247	8	Party	
66. Sentence	Head	Text	Context
321	27 (gov)	we	
322	10	We	
323	19	we	
323	27	we	
324	6	us	
324	14	our	
324	20	our	
324	25	Our	
324	29	our	
325	1	We	
326	1	We	
327	1	We	
330	14	we	
67. Sentence	Head	Text	Context
327	5 (gov)	the break of day	
322	14	break of day Far over wood and mountain	
334	9	the break of day	
404	8	the break of day	
68. Sentence	Head	Text	Context
104	9 (gov)	with a	
36	11	a passing	
69. Sentence	Head	Text	Context
36	16 (gov)	Mr. Frodo	
38	46	Mr. Frodo	
45	8	Frodo	
50	2	Frodo	
60	11	Frodo	
64	9	Frodo	
70	1	Frodo	
75	1	Frodo	
83	1	Frodo	
85	3	his	
85	14	him	
85	17	him	
86	12	he	
86	14	himself	
86	17	he	
87	7	his	
87	15	he	
87	19	he	
87	29	he	
90	1	he	
99	2	Frodo	
122	2	Frodo	
126	1	Frodo	
126	5	his	
127	19	he	
139	2	Frodo	
143	7	Frodo	
152	15	Frodo	
156	2	Frodo	
157	1	He	
159	1	Frodo	
161	9	Frodo	
162	2	Cousin Frodo	
163	7	him	
165	8	Frodo hastily	
170	9	Frodo	
170	14	his	
171	5	Frodo	
185	2	Frodo	
185	6	him	
186	7	Frodo	
191	1	Frodo	
191	3	his	
192	1	His	
193	3	old Frodo	
202	2	Frodo	
218	7	Frodo	
228	2	Frodo	
238	2	Frodo , now completely amazed	
238	2	Frodo	
245	2	Frodo	
246	7	his	
259	13	Frodo , that has ever seen the old fellow s secret book	
259	13	Frodo	
260	4	his	
261	2	Frodo	
269	6	Frodo	
272	1	He	
278	2	Frodo	
278	9	he	
285	2	Frodo , feeling that amazement could go no further , and quite unable to decide whether he felt angry , amused , relieved , or merely foolish	
285	2	Frodo	
285	18	he	
289	10	Mr. Frodo	
289	10	Frodo	
289	14	Mr.	
292	13	Frodo	
298	6	Frodo	
305	8	Frodo	
318	5	Captain Frodo	
318	5	Frodo	
319	8	him	
329	2	Frodo	
335	7	Frodo	
347	12	Frodo	
357	1	Frodo	
358	8	he	
370	9	Frodo	
381	1	Frodo	
387	6	Frodo	
390	10	Frodo	
393	2	Frodo , when he understood the plan	
393	2	Frodo	
393	5	he	
405	10	Frodo	
70. Sentence	Head	Text	Context
199	20 (gov)	your beloved Bag End	
36	25	Bag End	
85	35	Bag End	
271	6	Bag End	
71. Sentence	Head	Text	Context
38	8 (gov)	Pippin	
58	3	Pippin	
96	2	Pippin	
110	4	Pippin	
123	7	Pippin	
125	1	he	
137	2	Pippin	
152	8	Pippin	
155	9	Pippin	
162	9	Pippin	
194	2	Pippin	
223	2	Pippin	
320	3	Pippin	
333	2	Pippin	
398	8	Pippin	
72. Sentence	Head	Text	Context
38	16 (gov)	the path	
48	5	the path	
73. Sentence	Head	Text	Context
40	4 (gov)	the far stage	
40	19	it	
41	5	it	
42	1	It	
74. Sentence	Head	Text	Context
40	9 (gov)	the distant lamps	
42	15	the lamps	
75. Sentence	Head	Text	Context
41	12 (gov)	this way	
58	6	your way	
76. Sentence	Head	Text	Context
330	4 (gov)	that case	
332	1	That	
77. Sentence	Head	Text	Context
45	1 (gov)	Something that is following us	
47	2	s	
78. Sentence	Head	Text	Context
48	1 (gov)	They	
48	15	they	
79. Sentence	Head	Text	Context
336	5 (gov)	those Black Riders	
348	6	the Black Riders	
80. Sentence	Head	Text	Context
52	1 (gov)	They	
52	11	they	
81. Sentence	Head	Text	Context
52	5 (gov)	ten miles	
67	8	miles	
82. Sentence	Head	Text	Context
342	5 (gov)	we	
344	13	we	
83. Sentence	Head	Text	Context
56	7 (gov)	we	
59	1	We	
60	1	We	
60	3	our	
60	14	we	
84. Sentence	Head	Text	Context
58	17 (gov)	Fatty Bolger	
75	9	Fatty Bolger	
145	17	Fatty Bolger	
387	9	Fatty Bolger	
85. Sentence	Head	Text	Context
143	25 (gov)	the injured Maggot	
60	8	Farmer Maggot	
140	6	Mrs. Maggot , a queen	
140	6	Mrs. Maggot	
140	6	Maggot	
140	9	a queen	
150	21	old Maggot	
150	21	Maggot	
150	25	he	
151	1	He	
151	6	he	
160	31	Maggot	
164	14	Farmer Maggot	
166	1	Maggot	
167	2	Old Maggot	
167	6	a shrewd fellow	
168	6	his	
168	15	his	
169	5	he	
169	18	he	
86. Sentence	Head	Text	Context
60	4 (gov)	our supper early with Farmer Maggot	
61	4	it	
87. Sentence	Head	Text	Context
347	9 (gov)	a very efficient conspiracy	
349	2	it	
350	1	That	
88. Sentence	Head	Text	Context
64	14 (gov)	Crickhollow	
104	7	Crickhollow	
356	14	Crickhollow	
389	40	Crickhollow	
89. Sentence	Head	Text	Context
64	4 (gov)	some distance from the Brandywine to Frodo s new house at Crickhollow	
248	4	the distance	
411	8	the distance	
90. Sentence	Head	Text	Context
351	10 (gov)	course , if they were not stopped at the North-gate , where the Hedge runs down to the river-bank , just this side of the Bridge	
382	13	course , when the trees are sleepy and fairly quiet	
91. Sentence	Head	Text	Context
65	21 (gov)	the main road of Buckland that ran south from the Bridge	
247	17	the road	
253	7	the road	
92. Sentence	Head	Text	Context
66	1 (gov)	Half a mile	
67	10	it	
93. Sentence	Head	Text	Context
351	19 (gov)	the North-gate	
351	32	this side of the Bridge	
361	6	the North-gate	
361	28	it	
94. Sentence	Head	Text	Context
352	2 (gov)	The gate-guards	
352	6	them	
352	12	they	
353	5	they	
353	10	them	
353	20	they	
353	32	they	
353	48	them	
95. Sentence	Head	Text	Context
68	2 (gov)	last	
179	4	last	
180	5	it	
183	17	it	
324	19	last	
405	3	last	
96. Sentence	Head	Text	Context
68	8 (gov)	a narrow gate	
74	10	the gate	
97. Sentence	Head	Text	Context
73	6 (gov)	an old-fashioned countrified house	
69	7	the house in the dark	
69	12	it	
70	4	it	
70	7	it	
72	1	It	
73	1	It	
73	16	it	
73	28	it	
78	26	the house	
395	36	the house	
98. Sentence	Head	Text	Context
69	33 (gov)	low trees inside the outer hedge	
382	17	the trees	
99. Sentence	Head	Text	Context
355	8 (gov)	the morning even	
407	10	the morning	
100. Sentence	Head	Text	Context
355	20 (gov)	Mr. Baggins	
389	35	Mr. Baggins	
101. Sentence	Head	Text	Context
72	6 (gov)	a long	
209	7	long	
230	4	that long	
102. Sentence	Head	Text	Context
359	4 (gov)	tomorrow	
359	9	it	
397	10	tomorrow	
103. Sentence	Head	Text	Context
73	42 (gov)	a large round door	
75	5	the door	
75	11	it	
104. Sentence	Head	Text	Context
364	3 (gov)	The only thing to do	
365	2	that	
368	1	It	
370	1	It	
371	1	It	
371	5	the only way of getting off without being followed at once	
396	20	the way	
105. Sentence	Head	Text	Context
366	2 (gov)	Fredegar	
373	14	Fredegar	
384	2	Fredegar	
386	22	he	
386	30	he	
387	3	he	
388	1	His	
400	12	Fredegar	
106. Sentence	Head	Text	Context
367	1 (gov)	You	
367	8	that	
107. Sentence	Head	Text	Context
78	11 (gov)	either side	
79	8	it	
108. Sentence	Head	Text	Context
78	18 (gov)	a passage	
80	6	the passage	
93	6	the passage	
108	14	the passage	
109. Sentence	Head	Text	Context
81	1 (gov)	We	
81	4	our	
110. Sentence	Head	Text	Context
81	5 (gov)	our best	
81	12	it	
236	16	our best	
111. Sentence	Head	Text	Context
108	3 (gov)	Fatty	
82	3	Fatty and I	
396	3	Fatty	
112. Sentence	Head	Text	Context
82	13 (gov)	the last cart-load yesterday	
84	1	It	
113. Sentence	Head	Text	Context
85	8 (gov)	Bilbo	
164	25	Bilbo	
172	6	Bilbo	
172	26	him	
174	1	He	
174	12	he	
175	5	him	
212	3	Bilbo	
213	16	he	
214	7	him	
215	23	he	
241	13	Bilbo	
241	22	he	
242	5	Bilbo , of course	
242	5	Bilbo	
242	23	he	
242	29	he	
247	22	Bilbo	
250	1	Bilbo	
251	1	he	
254	10	Bilbo when he suddenly reappeared	
255	8	he	
255	13	his	
321	12	Bilbo	
321	14	his	
114. Sentence	Head	Text	Context
85	1 (gov)	Many of his own favourite things or Bilbo	
85	12	they	
85	19	their	
85	30	they	
87	27	them	
87	32	them	
88	13	they	
115. Sentence	Head	Text	Context
85	6 (gov)	his own favourite things	
102	5	things	
102	8	that	
198	22	things	
273	22	things	
116. Sentence	Head	Text	Context
85	21 (gov)	their new setting	
86	1	It	
86	4	a pleasant , comfortable , welcoming place	
86	4	a pleasant	
87	1	It	
88	2	that	
89	1	It	
117. Sentence	Head	Text	Context
87	8 (gov)	his friends	
219	8	dear friends	
220	5	us all	
240	14	friends	
298	1	We	
298	4	your friends	
118. Sentence	Head	Text	Context
88	10 (gov)	night	
352	9	night	
119. Sentence	Head	Text	Context
92	2 (gov)	The travellers	
92	5	their	
92	10	their	
120. Sentence	Head	Text	Context
100	2 (gov)	first	
100	6	first	
243	8	first	
397	8	first	
412	2	first	
412	5	it	
412	9	a great wind coming over the leaves of the forest	
413	5	it	
121. Sentence	Head	Text	Context
394	1 (gov)	We	
396	21	we	
399	8	our	
122. Sentence	Head	Text	Context
131	15 (gov)	Peregrin	
101	4	last either way	
101	9	Master Peregrin	
131	17	he	
123. Sentence	Head	Text	Context
108	7 (gov)	the kitchen	
127	15	the kitchen	
133	6	the kitchen	
124. Sentence	Head	Text	Context
401	13 (gov)	this time tomorrow	
402	1	It	
402	7	it	
125. Sentence	Head	Text	Context
172	19 (gov)	perhaps one	
110	13	one of Bilbo s favourite bath-songs	
126. Sentence	Head	Text	Context
112	7 (gov)	day	
327	7	day	
334	11	day	
404	10	day	
127. Sentence	Head	Text	Context
403	1 (gov)	We	
403	18	we	
128. Sentence	Head	Text	Context
113	2 (gov)	A loon	
113	4	he that will not sing : O	
129. Sentence	Head	Text	Context
114	2 (gov)	Water Hot	
114	6	a noble thing	
118	29	Water Hot	
130. Sentence	Head	Text	Context
115	1 (gov)	O !	
117	1	O !	
119	1	O !	
131. Sentence	Head	Text	Context
405	4 (gov)	he	
406	1	His	
407	1	He	
407	5	he	
408	2	he	
408	11	he	
410	1	He	
410	7	him	
411	2	he	
412	3	he	
413	2	he	
414	2	he	
414	4	he	
416	1	He	
417	3	he	
417	6	him	
418	6	him	
419	1	He	
132. Sentence	Head	Text	Context
118	3 (gov)	we	
118	24	we	
133. Sentence	Head	Text	Context
118	11 (gov)	a thirsty throat	
124	14	the throat	
134. Sentence	Head	Text	Context
118	20 (gov)	Beer	
118	18	better	
135. Sentence	Head	Text	Context
408	24 (gov)	a dark sea of tangled trees	
418	14	the Sea	
136. Sentence	Head	Text	Context
408	27 (gov)	tangled trees	
415	4	no trees	
137. Sentence	Head	Text	Context
120	10 (gov)	a fountain	
123	13	a fountain	
138. Sentence	Head	Text	Context
120	14 (gov)	the sky	
419	19	the sky	
139. Sentence	Head	Text	Context
409	11 (gov)	creatures crawling and snuffling	
410	4	they	
140. Sentence	Head	Text	Context
121	5 (gov)	a terrific splash	
123	1	It	
141. Sentence	Head	Text	Context
413	12 (gov)	the sound of the Sea far-off ; a sound	
413	29	it	
142. Sentence	Head	Text	Context
413	16 (gov)	the Sea far-off ; a sound	
413	20	he	
413	33	his	
143. Sentence	Head	Text	Context
127	8 (gov)	the air	
416	17	the air	
144. Sentence	Head	Text	Context
417	10 (gov)	a tall white tower	
417	12	standing alone on a high ridge	
418	10	the tower	
419	10	the tower	
145. Sentence	Head	Text	Context
133	9 (gov)	a table near the fire	
146	5	the table	
148	6	it	
146. Sentence	Head	Text	Context
133	12 (gov)	the fire	
146	12	the fire	
152	19	the fire	
147. Sentence	Head	Text	Context
134	8 (gov)	mushrooms	
142	6	mushrooms	
148. Sentence	Head	Text	Context
140	12 (gov)	farmers wives	
141	11	them	
149. Sentence	Head	Text	Context
142	15 (gov)	Big People	
376	1	People	
377	3	they	
150. Sentence	Head	Text	Context
143	8 (gov)	young Frodo s	
145	12	they	
146	1	They	
249	1	s	
254	1	s	
398	4	s	
151. Sentence	Head	Text	Context
143	14 (gov)	the renowned fields of the Marish	
346	10	the fields	
152. Sentence	Head	Text	Context
144	3 (gov)	this occasion	
320	16	the occasion	
153. Sentence	Head	Text	Context
150	18 (gov)	the matter with old Maggot	
289	18	that matter	
154. Sentence	Head	Text	Context
350	9 (gov)	the Riders	
153	18	Black Riders	
154	4	they	
157	8	their	
157	14	they	
172	13	the Riders	
174	15	they	
341	5	the Riders	
350	14	they	
351	1	They	
351	13	they	
353	40	the Riders	
368	8	Black Riders	
395	7	these Riders	
395	30	they	
399	17	Black Riders	
155. Sentence	Head	Text	Context
156	5 (gov)	t talk	
168	16	his talk	
156. Sentence	Head	Text	Context
157	12 (gov)	the time when they left Hobbiton	
163	3	the time	
164	18	it	
165	1	That	
165	5	a guess	
157. Sentence	Head	Text	Context
158	3 (gov)	various supporting	
160	7	it	
161	6	it	
158. Sentence	Head	Text	Context
168	2 (gov)	A lot	
282	5	a lot	
159. Sentence	Head	Text	Context
170	2 (gov)	you	
170	7	us , Frodo , whether you think his guess good or bad	
160. Sentence	Head	Text	Context
171	13 (gov)	a good guess	
171	18	it	
161. Sentence	Head	Text	Context
172	4 (gov)	a connexion with Bilbo s old adventures	
173	12	it	
173	15	no joke at all	
176	1	It	
162. Sentence	Head	Text	Context
179	2 (gov)	Frodo at last	
179	10	his	
179	15	he	
163. Sentence	Head	Text	Context
186	16 (gov)	my dear old Frodo : you are miserable , because you don t	
186	2	this	
208	2	Don t	
208	5	that	
164. Sentence	Head	Text	Context
188	23 (gov)	once	
361	15	once	
371	13	once	
381	4	once	
165. Sentence	Head	Text	Context
191	4 (gov)	his mouth	
191	7	it	
166. Sentence	Head	Text	Context
195	11 (gov)	our	
198	1	We	
167. Sentence	Head	Text	Context
196	1 (gov)	You	
196	11	that	
168. Sentence	Head	Text	Context
200	7 (gov)	Gandalf	
204	6	Gandalf	
275	12	Gandalf	
300	6	Gandalf	
338	9	Gandalf	
339	13	Gandalf	
349	10	Gandalf	
386	15	Gandalf what you have done	
394	10	Gandalf	
169. Sentence	Head	Text	Context
201	2 (gov)	Good heavens	
262	2	Good heavens	
170. Sentence	Head	Text	Context
206	2 (gov)	Oh no	
270	1	No	
271	1	It	
272	5	it	
290	18	no	
171. Sentence	Head	Text	Context
209	2 (gov)	The secret	
209	15	it	
172. Sentence	Head	Text	Context
209	25 (gov)	known to us conspirators	
389	11	the conspirators	
391	1	They	
173. Sentence	Head	Text	Context
209	24 (gov)	us	
210	8	we	
211	1	We	
214	22	we	
215	1	We	
215	9	us	
216	5	we	
216	8	our	
216	20	our	
174. Sentence	Head	Text	Context
216	4 (gov)	this spring	
219	1	It	
220	1	It	
273	20	this spring when things got serious	
175. Sentence	Head	Text	Context
216	9 (gov)	our eyes	
256	6	my eyes	
176. Sentence	Head	Text	Context
226	12 (gov)	a dragon s throat to save you	
229	6	it	
177. Sentence	Head	Text	Context
226	40 (gov)	your dangerous adventure	
321	15	his adventure	
178. Sentence	Head	Text	Context
232	8 (gov)	no there-and-back journey	
232	1	This	
232	4	no treasure-hunt , no there-and-back journey	
232	4	no treasure-hunt	
324	26	Our journey	
385	37	the journey	
179. Sentence	Head	Text	Context
234	3 (gov)	we	
235	1	That	
235	4	we	
236	1	We	
236	10	we	
236	15	our	
180. Sentence	Head	Text	Context
236	4 (gov)	the Ring	
236	7	no laughing-matter	
237	2	The Ring !	
239	3	the Ring	
301	8	the Ring	
181. Sentence	Head	Text	Context
241	6 (gov)	the existence of the Ring for years	
241	25	it	
182. Sentence	Head	Text	Context
241	32 (gov)	the knowledge	
273	14	my knowledge	
183. Sentence	Head	Text	Context
241	38 (gov)	we	
241	40	our	
184. Sentence	Head	Text	Context
248	6 (gov)	the S.-B .	
253	11	the S.-B	
254	12	he	
185. Sentence	Head	Text	Context
255	14 (gov)	his trouser-pocket	
258	6	it	
186. Sentence	Head	Text	Context
259	24 (gov)	the old fellow s secret book	
260	5	his book	
266	5	the book about	
267	6	it	
			
