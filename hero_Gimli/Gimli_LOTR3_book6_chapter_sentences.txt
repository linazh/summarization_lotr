


And when they were arrayed they went to the great feast; and they sat at the King's table with Gandalf, and King Eomer of Rohan, and the Prince Imrahil and all the chief captains; and there also were Gimli and Legolas.  At last the glad day ended; and when the Sun was gone and the round Moon rode slowly above the mists of Anduin and flickered through the fluttering leaves, Frodo and Sam sat under the whispering trees amid the fragrance of fair Ithilien; and they talked deep into the night with Merry and Pippin and Gandalf, and after a while Legolas and Gimli joined them.  That you certainly are not,' said Gimli.  And not only Sam and Frodo here,' said Gimli, 'but you too, Pippin. 

And before he went to his rest he sent for Gimli the Dwarf, and he said to him: 'Gimli Gloin's son, have you your axe ready?  Nay, lord,' said Gimli, 'but I can speedily fetch it, if there be need.  Well, lord,' said Gimli, 'and what say you now?  Then I must go for my axe,' said Gimli.  Then Gimli bowed low.  Nay, you are excused for my part, lord,' he said.  For the other Companions steeds were furnished according to their stature; and Frodo and Samwise rode at Aragorn's side, and Gandalf rode upon Shadowfax, and Pippin rode with the knights of Gondor; and Legolas and Gimli as ever rode together upon Arod.  Then Legolas repaid his promise to Gimli and went with him to the Glittering Caves; and when they returned he was silent, and would say only that Gimli alone could find fit words to speak of them.  Come, Gimli!  To this Gimli agreed, though with no great delight, it seemed.  We will come, if our own lords allow it,' said Gimli. 



