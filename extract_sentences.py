file_name = input()
ch_num = int(input())
#file_name = "_LOTR1_chapter"

for i in range(ch_num):
    f_name = file_name + str(i + 1)
    fin = open("out_files/" + f_name + ".txt.out", 'r')
    fout = open("sentences_from_chapters/sentences_" + f_name + ".txt", 'w')

    line = '...'
    while line != '':
        line = fin.readline()
        arr = line.split()
        if (line == '' or line == '\n' or line[0] == '[' or
            (len(arr) == 3 and arr[0] == 'Sentence' and arr[1][0] == '#')):
            continue
        fout.write(line)
    fout.close()
    fin.close()
